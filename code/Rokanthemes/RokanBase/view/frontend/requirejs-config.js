var config = {
	"shim": {
		"rokanthemes/owl": ["jquery"],
		"rokanthemes/choose": ["jquery"],
		"rokanthemes/jquery_sameheight": ["jquery"],
		"rokanthemes/jquery_sameminheight": ["jquery"],
		"rokanthemes/lazyloadimg": ["jquery"]
	},
	'paths': {
		"rokanthemes/owl": "Rokanthemes_RokanBase/js/owl_carousel",
		"rokanthemes/choose": "Rokanthemes_RokanBase/js/jquery_choose",
		"rokanthemes/jquery_sameheight": "Rokanthemes_RokanBase/js/jquery_sameheight",
		"rokanthemes/jquery_sameminheight": "Rokanthemes_RokanBase/js/jquery_sameminheight",
		'rokanthemes/lazyloadimg': 'Rokanthemes_RokanBase/js/jquery.lazyload.min'
	},
	"deps": ['rokanthemes/theme']
};
